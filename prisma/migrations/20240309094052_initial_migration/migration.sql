-- CreateTable
CREATE TABLE "XmrSetting" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "currentSubaddressIndex" INTEGER NOT NULL DEFAULT 1,
    "restoreHeight" INTEGER NOT NULL DEFAULT 2000000
);

-- CreateTable
CREATE TABLE "Transaction" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "transactionKey" TEXT,
    "amount" REAL DEFAULT 0,
    "isConfirmed" BOOLEAN NOT NULL DEFAULT false,
    "isUnlocked" BOOLEAN NOT NULL DEFAULT false,
    "itemId" TEXT,
    CONSTRAINT "Transaction_itemId_fkey" FOREIGN KEY ("itemId") REFERENCES "Item" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Item" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "moneroSubaddress" TEXT,
    "payedAmount" REAL DEFAULT 0,
    "expectedAmount" REAL DEFAULT 0,
    "isPayed" BOOLEAN DEFAULT false
);

-- CreateIndex
CREATE UNIQUE INDEX "Transaction_transactionKey_key" ON "Transaction"("transactionKey");

-- CreateIndex
CREATE UNIQUE INDEX "Item_moneroSubaddress_key" ON "Item"("moneroSubaddress");
