// NOTE: for ui components that repeat
// keep these "dump", i.e. just pass in the data as props

export default function SampleSharedComponent({
  label,
  value,
}: {
  label: string;
  value: string;
}) {
  const shortenedValue = `${value.slice(0, 6)}...${value.slice(value.length - 6, value.length)}`;
  return (
    <div className="border p-4 text-center ">
      <p className="font-semibold tracking-wide">{label}</p>
      <pre>{shortenedValue}</pre>
    </div>
  );
}
