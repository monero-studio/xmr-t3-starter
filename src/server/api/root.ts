import { serverWalletRouter } from "~/server/api/routers/serverWallet";
import { createTRPCRouter } from "~/server/api/trpc";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  serverWallet: serverWalletRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
