import { z } from "zod";

import { createTRPCRouter, publicProcedure } from "~/server/api/trpc";

export const serverWalletRouter = createTRPCRouter({
  createNewSubaddress: publicProcedure.mutation(async ({ ctx }) => {
    // NOTE: we can get the serverWallet out of the context
    const subaddress = await ctx.serverWallet.createSubaddress(0);
    return {
      newAddress: subaddress.getAddress(),
    };
  }),

  // create: publicProcedure
  //   .input(z.object({ name: z.string().min(1) }))
  //   .mutation(async ({ ctx, input }) => {
  //     // simulate a slow db call
  //     await new Promise((resolve) => setTimeout(resolve, 1000));

  //     return ctx.db.post.create({
  //       data: {
  //         name: input.name,
  //       },
  //     });
  //   }),

  // getLatest: publicProcedure.query(({ ctx }) => {
  //   return ctx.db.post.findFirst({
  //     orderBy: { createdAt: "desc" },
  //   });
  // }),
});
