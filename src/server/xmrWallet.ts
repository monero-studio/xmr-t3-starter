import * as moneroTs from "monero-ts";
import { revalidatePath } from "next/cache";
import { env } from "~/env";
import { db } from "./db";

export async function initWallet() {
  const walletRpc = await moneroTs.connectToWalletRpc({
    uri: process.env.MONERO_RPC_URI,
    rejectUnauthorized: false,
  });

  // TODO set onBalanceReceived listener and credit the appropriate account

  const wallet = await walletRpc.openWallet({
    path: env.MONERO_SERVER_WALLET_PATH,
    password: env.MONERO_SERVER_WALLET_PASSWORD,
  });
  await wallet.addListener(
    new (class extends moneroTs.MoneroWalletListener {
      async onOutputReceived(output: moneroTs.MoneroOutputWallet) {
        // --- Parse the transaction
        const targetAddressIndex = output.getSubaddressIndex();
        const subaddress = (
          await wallet.getSubaddress(0, targetAddressIndex)
        ).getAddress();
        const amount = convertBigIntToXmrFloat(output.getAmount());
        const transactionKey = output.getKeyImage().getHex();
        const isConfirmed = output.getTx().getIsConfirmed();
        const isLocked = output.getTx().getIsLocked();

        // --- Existing Tx or new?
        const existingTx = await db.transaction.findFirst({
          where: { transactionKey },
        });

        await db.transaction.upsert({
          where: { transactionKey: transactionKey },
          update: { isConfirmed, isUnlocked: !isLocked },
          create: {
            // NOTE: the transaction key is unique per transaction
            // transactions are going through the outputReceivedListener multiple times
            // 1. when they are first received by the wallet and in a locked state
            // 2. when they are unlocked and spendable again
            // this logic helps us keep track and not duplicate transactions
            transactionKey: transactionKey,
            isConfirmed,
            isUnlocked: !isLocked,
            amount: Number(amount),
            // NOTE: if the transaction is relation to another item,
            // you can see ever tx for every item
            // itemId: relatedItem?.id,
          },
        });

        if (existingTx) return;
        // --- Update some product item in the database
        // await updateItemWithTx(relatedItem, amount);

        // --- Finishing up
        await wallet.save();
      }
    })(),
  );
  return wallet;
}

// OPTIONAL
async function updateItemWithTx(item: any | null, amount: number) {
  // NOTE: this is an example of some business logic being triggered when a new tx is recorded
  // new tx was received, update the amount of the  item + unlocking logic
  if (!item) return;

  const newAmount = (item?.payedAmount ?? 0) + amount;

  const isUnlocked = !!item?.goalAmount && item?.goalAmount <= newAmount;

  await db.Item.update({
    where: { id: item.id },
    data: {
      isUnlocked,
      goalReached: isUnlocked,
      payedAmount: newAmount,
      lastDonationTimestamp: new Date(),
      releaseDate: isUnlocked ? new Date() : null,
    },
  });

  revalidatePath("/");
}

export async function getFreshSubaddress(
  moneroWallet: moneroTs.MoneroWalletRpc,
): Promise<string> {
  // NOTE: helper if you need to keep track of your subaddress indexes in a database
  // database entry may be useful, since your wallet file lost and you need to restore from seed
  const { currentSubaddressIndex } = await db.xmrSetting.findFirstOrThrow({
    where: { id: 1 },
  });

  const newSubaddress = moneroWallet.getSubaddress(0, currentSubaddressIndex);
  const newIndex = currentSubaddressIndex + 1;

  await db.xmrSetting.update({
    where: { id: 1 },
    data: {
      currentSubaddressIndex: newIndex,
    },
  });

  return (await newSubaddress).getAddress();
}

export function convertBigIntToXmrFloat(amount: bigint) {
  // NOTE: helper for converting monero amounts
  return parseFloat((Number(amount) / 1000000000000).toFixed(12));
}

export function calculateDeltaToGoal(amount = 0, goal: number | null) {
  // NOTE: helper for checking on a human readable goal (i.e. 0.012234 xmr)
  if (!goal) return 0;
  return parseFloat(
    ((goal * 1000000000000 - amount * 1000000000000) / 1000000000000).toFixed(
      12,
    ),
  );
}

// NOTE: 💡 The singleton pattern helps us only have one active instance of the walletRPC
// use the singleton with:
//
// await WalletSingleton.getInstance()
//

export class WalletSingleton {
  static wallet: moneroTs.MoneroWalletRpc;

  static async getInstance() {
    if (!WalletSingleton.wallet) {
      WalletSingleton.wallet = await initWallet();
    }
    return WalletSingleton.wallet;
  }
}
