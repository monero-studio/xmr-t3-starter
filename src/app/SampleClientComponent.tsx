"use client";

import { toast } from "sonner";
import SampleSharedComponent from "~/components/SampleSharedComponent";
import { Button } from "~/components/ui/button";
import { api } from "~/trpc/react";

export function SampleClientComponent() {
  // NOTE: this is a client component, if you need some stuff from the server (like authSession)
  // you can pass it as props
  const { mutate, data, isLoading } =
    api.serverWallet.createNewSubaddress.useMutation({
      onSuccess: () => {
        toast(
          "New subaddress generated on the server, requested by client component",
        );
      },
    });
  return (
    <>
      <Button
        onClick={() => {
          toast("Requesting new subaddress from client");
          mutate();
        }}
        disabled={isLoading}
      >
        Request a new subaddress from client
      </Button>
      {!!data?.newAddress ? (
        <SampleSharedComponent
          label="New subaddress requested by client:"
          value={data.newAddress}
        />
      ) : null}
    </>
  );
}
