import { unstable_noStore as noStore } from "next/cache";
import { SampleClientComponent } from "./SampleClientComponent";
import { api } from "~/trpc/server";
import { Separator } from "~/components/ui/separator";
import MaxWidthWrapper from "~/components/MaxWidthWrapper";
import SampleSharedComponent from "~/components/SampleSharedComponent";

export default async function Home() {
  noStore();
  // NOTE: here in the server component we can execute async call from our server,
  // like calling the database, or some other backend
  const { newAddress } = await api.serverWallet.createNewSubaddress.mutate();

  return (
    <MaxWidthWrapper className="flex flex-col items-center justify-center gap-8">
      <h1 className="text-3xl font-bold tracking-tight">XMR-T3 starter 🧑‍🚀🚀</h1>
      <SampleSharedComponent
        label="New subaddress generated on server:"
        value={newAddress}
      />
      <Separator />
      <SampleClientComponent />
    </MaxWidthWrapper>
  );
}
